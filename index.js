console.log("Hello World!");

let firstName = "Dennis", lastName = "Orpina", age = "37";
let hobbies = ["biking", "playing chess", "drinking"];
let isMarried = true;
let workAddress = {
	houseNumber: "33",
	street:  "Washington",
	city:  "Lincoln",
	state: "Nebraska"
}


function printUserInfo(){
	console.log("First Name: " +firstName);
	console.log("Last Name: " +lastName);
	console.log("Age: " +age);
	console.log("Hobbies:");
	console.log(hobbies);
	console.log("Work Address:");
	console.log(workAddress);
};

printUserInfo(firstName, lastName, age, hobbies, workAddress);

function returnFunction() {
	return firstName + lastName + age + hobbies + workAddress;
};

console.log(firstName + " " + lastName + " is " + age + "years of age.");
console.log("This was printed inside of the function.");

console.log(hobbies);
console.log("This was printed inside of the function.");


console.log(workAddress);
console.log("The value of isMarried is: " +isMarried);




